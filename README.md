# PXE Web

This is a Python Flask application that hosts a webinterface to manage a PXE environment
based on ipxe. The webserver is not only used for hosting the admin panel but also the 
dynamic ipxe configuration.

## Features

* WebUI to create boot menu options
* Show/hide specific boot menu options for specific machines
* Automatically use the newest kernel/initramfs from the NFS rootfs so upgrades work on RW images.
* Bootstrap PXE-bootable installations with debootstrap (todo)
* Includes all files to run a PXE server, including Memtest86+ image

## Setup

PXE Web depends on an external DHCP server that points to the host running pxeweb with
the filename `undionly.kpxe`. It also requires you to run a tftp server on the `static/tftp`
folder in this repository. The bundled ipxe binary has an embedded ipxe script that fetches
the configuration through http. This requires that the name `pxeweb` resolves in DNS to the
machine that runs the PXE Web installation.

```shell-session
$ apt install tftpd-hpa
$ git clone https://gitlab.com/MartijnBraam/pxeweb.git
$ cd pxeweb
$ vim /etc/default/tftpd-hpa
# Change the TFTP_DIRECTORY value to the the /static/tftp directory in this repository
$ FLASK_APP=app.py flask run -h 0.0.0.0 -p 4793
```

If you want to use NFS for the rootfs of an operating system then you'll need to add an NFS
export for the `static/nfs` directory in this repository. Here is an example NFS export line:

```
/opt/pxeweb/static/nfs *(rw,no_root_squash,no_subtree_check)
```

## Boot process

1. The clients PXE implementation does a DHCP request
1. The clients PXE implementation uses the `next-server` and `filename` DHCP parameter to fetch `undionly.kpxe`
1. ipxe (undionly) executes its embedded ipxe script
1. The embedded script does dhcp again with ipxe's client to ensure we have functional DNS
1. The script fetches another ipxe script through http from `http://pxeweb:4793` which is the flask application
1. The flask application dynamically generates an ipxe script for the UUID of the machine. This script contains
the graphical menu
1. The user chooses one of the options in the menu and it gets fetched over http by ipxe