import os
import glob
import subprocess
import json

from flask import Flask, render_template, make_response, g, request, redirect, url_for
import sqlite3

app = Flask(__name__)

DATABASE = 'database.db'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        c = db.cursor()
        c.execute('PRAGMA foreign_keys = ON;')

    db.row_factory = sqlite3.Row
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.before_first_request
def init_database():
    if os.path.isfile(DATABASE):
        return
    print('Creating initial database...')
    db = sqlite3.connect(DATABASE)
    c = db.cursor()
    c.execute('CREATE TABLE machines ('
              'id integer primary key,'
              'uuid text not null,'
              'label text not null'
              ');')
    c.execute('CREATE UNIQUE INDEX machines_uuid_uindex ON machines(uuid);')
    c.execute('CREATE TABLE bootables ('
              'id integer primary key,'
              'code text not null,'
              'label text not null,'
              'boottype text not null,'
              'image text,'
              'ramdisk text,'
              'initiator text,'
              'target text,'
              'target_ip text,'
              'options text,'
              'everywhere integer,'
              'locked integer,'
              'foreign key(locked) references machines(id)'
              ');')
    c.execute('CREATE UNIQUE INDEX bootables_code_uindex ON bootables(code);')
    c.execute('CREATE TABLE machine_bootables ('
              'machineid integer,'
              'bootableid integer,'
              'foreign key(machineid) references machines(id),'
              'foreign key(bootableid) references bootables(id)'
              ');')
    db.close()


def query_db(query, args=(), one=False, commit=False):
    cur = get_db().execute(query, args)
    if commit:
        get_db().commit()
        cur.close()
        return
    else:
        rv = cur.fetchall()
        cur.close()
        return (rv[0] if rv else None) if one else rv


def get_newest_kernel(dir, prefix='vmlinuz'):
    dir = os.path.join(os.path.abspath('static'), dir)
    kernels = glob.glob(os.path.join(dir, '{}-*'.format(prefix)))
    kvers = []
    for kernel in kernels:
        kver = kernel.replace(os.path.join(dir, '{}-'.format(prefix)), '')
        kvers.append(kver)
    kvers = list(reversed(sorted(kvers)))
    if len(kvers) == 0:
        return ''
    return kvers[0]


app.jinja_env.globals.update(get_newest_kernel=get_newest_kernel)


def iscsi_in_use(iqn):
    raw = subprocess.check_output('targetcli sessions detail', universal_newlines=True, shell=True)
    return iqn in raw


def iscsi_get_targets():
    with open('/etc/target/saveconfig.json') as handle:
        raw = handle.read()
    saveconfig = json.loads(raw)
    targets = saveconfig['targets']
    result = []
    for target in targets:
        iqn = target['wwn']
        label = []
        for tpg in target['tpgs']:
            for lun in tpg['luns']:
                label.append(lun['storage_object'].replace('/backstores/', ''))

        result.append({
            'iqn': iqn,
            'label': ', '.join(label)
        })
    return result


def iscsi_get_initiators():
    with open('/etc/target/saveconfig.json') as handle:
        raw = handle.read()
    saveconfig = json.loads(raw)
    targets = saveconfig['targets']
    result = set()
    for target in targets:
        for tpg in target['tpgs']:
            for acl in tpg['node_acls']:
                result.add(acl['node_wwn'])
    return list(result)


@app.route('/')
def index():
    bootables = query_db('SELECT * FROM bootables ORDER BY code ASC')
    machines = query_db('SELECT * FROM machines ORDER BY label ASC')
    return render_template('index.html', bootables=bootables, machines=machines)


@app.route('/bootable/add/ubuntu-nfs', methods=['GET', 'POST'])
def bootable_add_ubuntu_nfs():
    if request.method == 'POST':
        query_db('INSERT INTO bootables (code, label, boottype, everywhere) VALUES (?,?,?,?);', [
            request.form['code'],
            request.form['label'],
            'ubuntu_nfs',
            1 if 'everywhere' in request.form else 0
        ], commit=True)
        return redirect(url_for('index'))
    return render_template('add_bootable_ubuntu_nfs.html')


@app.route('/bootable/add/ubuntu-iscsi', methods=['GET', 'POST'])
def bootable_add_ubuntu_iscsi():
    if request.method == 'POST':
        query_db(
            'INSERT INTO bootables (code, label, boottype, everywhere, image, ramdisk, initiator, target,target_ip) VALUES (?,?,?,?,?,?,?,?,?);',
            [
                request.form['code'],
                request.form['label'],
                'ubuntu_iscsi',
                1 if 'everywhere' in request.form else 0,
                request.form['image'],
                request.form['ramdisk'],
                request.form['initiator'],
                request.form['target'],
                request.form['target_ip'],
            ], commit=True)
        return redirect(url_for('index'))

    static = os.path.abspath('static')
    targets = iscsi_get_targets()
    initiators = iscsi_get_initiators()
    return render_template('add_bootable_ubuntu_iscsi.html', static=static, targets=targets, initiators=initiators)


@app.route('/ipxe/boot/<string:uuid>')
def get_boot_script(uuid):
    machine = query_db('SELECT * FROM machines WHERE uuid=?', [uuid], one=True)
    name = "new machine"
    if machine is not None:
        name = machine['label']

    items = query_db('SELECT * FROM bootables WHERE everywhere = 1')

    specifics = query_db("""SELECT bootables.* FROM bootables
                            JOIN machine_bootables ON machine_bootables.bootableid = bootables.id
                            JOIN machines on machine_bootables.machineid = machines.id
                            WHERE machines.uuid = ?""", [uuid])

    items.extend(specifics)

    items = sorted(items, key=lambda k: k['code'])
    items = list(map(dict, items))

    for item in items:
        item['disabled'] = False
        if 'iscsi' in item['boottype']:
            if iscsi_in_use(item['initiator']):
                item['label'] += ' [In use]'
                item['disabled'] = True

    nfsroot = os.path.abspath('static/nfs')

    ipxe = render_template('bootmenu.ipxe.j2', items=items, nfsroot=nfsroot, name=name)
    response = make_response(ipxe)
    response.headers['Content-Type'] = 'text/plain'
    return response


if __name__ == '__main__':
    if not os.path.isfile(DATABASE):
        init_database()
    app.run()
