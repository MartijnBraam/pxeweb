#!/usr/bin/env bash
wget http://www.memtest.org/download/5.01/memtest86+-5.01.tar.gz
tar -xvf memtest86+-5.01.tar.gz
cd memtest86+-5.01
make memtest
cd ..
cp memtest86+-5.01/memtest memtest86+.elf
rm -rf memtest86+-5.01
rm -rf memtest86+-5.01.tar.gz