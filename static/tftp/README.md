##rom-o-matic template
https://rom-o-matic.eu/build.fcgi?BINARY=ipxe.kpxe&BINDIR=bin&REVISION=master&DEBUG=&EMBED.00script.ipxe=%23%21ipxe%0A%0Adhcp%0Achain%20http%3A//pxeweb%3A4793/ipxe/boot/%24%7Buuid%7D&general.h/NET_PROTO_LACP:=0&general.h/PXE_STACK:=1&general.h/PXE_MENU:=1&general.h/SANBOOT_PROTO_ISCSI:=1&general.h/SANBOOT_PROTO_HTTP:=1&branding.h/PRODUCT_NAME=PXE%20Web&

## Embedded ipxe script

```
#!ipxe

dhcp
chain http://pxeweb:4793/ipxe/boot/${uuid}
```