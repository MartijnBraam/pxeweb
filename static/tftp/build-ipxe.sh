#!/usr/bin/env bash
git clone git://git.ipxe.org/ipxe.git
cd ipxe/src
make bin/undionly.kpxe EMBED=../../embedded.ipxe
cp bin/undionly.kpxe ../../undionly.kpxe
cd ../..
rm -rf ipxe